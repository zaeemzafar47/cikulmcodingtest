//
//  CCTSearchImagesViewController.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import UIKit
import MBProgressHUD
import CRRefresh

class CCTSearchImagesViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var imagesTableView: UITableView!
    
    // MARK: - Public properties
    lazy var pxImageInfo = [Hits]()
    
    // MARL: - Private properties
    private let searchController = UISearchController(searchResultsController: nil)
    lazy private var searchTimer: Timer = Timer()
    lazy var metadata = APIMetadata()
    
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        defaultSearchAction()
    }
    
    // MARK: - Initial state setup
    func setupView() {
        title = "Search Images"
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        setupSearchController()
        configureTableViewForPagination()
    }
    
    func setupSearchController() {
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search images"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.delegate = self
    }
    
    func defaultSearchAction() {
        searchController.searchBar.text = "Fruits"
        getSearchResults()
    }
    
}

// MARK: - Table data source
extension CCTSearchImagesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pxImageInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CCTImageInfoCell.self)) as? CCTImageInfoCell, let cellData = pxImageInfo[safe: indexPath.row] else { return UITableViewCell() }
        
        cell.configureCell(data: cellData)
        
        return cell
    }
}

// MARK: - Table delegate
extension CCTSearchImagesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        UIAlertController.showAlert(inViewController: self, title: "Confirmation", message: "Are you sure you want to see the detail info of selected item", okButtonTitle: "YES", cancelButtonTitle: "NO") { [weak self] (success) in
        
            if success {
                if let itemDetailVC = CCTConstants.Storyboards.Main.instantiateViewController(withIdentifier: String(describing: CCTItemDetailViewController.self)) as? CCTItemDetailViewController {
                    itemDetailVC.itemDetail = self?.pxImageInfo[indexPath.row]
                    
                    self?.navigationController?.pushViewController(itemDetailVC, animated: true)
                }
            }
            
        }
        
    }
}

// MARK: - Search bar handler
extension CCTSearchImagesViewController: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        print("Update search results: \(String(describing: searchController.searchBar.text))")
    }
    
    
    // Created with 'objc' target so that this method can be accessed by Timer: it's still required
    @objc func getSearchResults() {
        getSearchResults(completionHandler: nil)
    }
    
    func getSearchResults(completionHandler: CompletionBlockWithBool? = nil) {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = "Searching..."

        let searchText = searchController.searchBar.text ?? ""
        let searchEndPoint = CCTUserEndpoint.searchImage(searchText: searchText, pageNumber: metadata.currentPage)

        CCTAPIManager.shared.sendRequest(apiConfig: searchEndPoint) { [weak self] (response: CCTAPIResponse<PixabayImageAPIResponse>) in
            guard let strongSelf = self else {
                completionHandler?(response.success)
                return
            }
            
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
            
            if response.success {
                guard let hits = response.data?.hits else { return }
                strongSelf.metadata.totalPages = response.data?.getMetadataInfo().totalPages ?? 1
                strongSelf.pxImageInfo.append(contentsOf: hits)
                strongSelf.imagesTableView.reloadData()
            } else {
                UIAlertController.showOkayAlert(inViewController: strongSelf, message: response.message, title: "API Response")
            }
            
            completionHandler?(response.success)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Search text changed \(searchText)")
        metadata.resetMetadata()
        pxImageInfo.removeAll()
        
        self.searchTimer.invalidate()
        self.searchTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getSearchResults), userInfo: nil, repeats: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        metadata.resetMetadata()
        pxImageInfo.removeAll()
        
        searchBar.text = ""
        
        self.searchTimer.invalidate()
        self.searchTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getSearchResults), userInfo: nil, repeats: false)
    }
    
}

//MARK:- Table Pagination
extension CCTSearchImagesViewController {
    
    func configureTableViewForPagination() {
        self.imagesTableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            guard let strongSelf = self else {return}
            strongSelf.metadata.resetMetadata()
            strongSelf.imagesTableView.cr.resetNoMore()
            strongSelf.pxImageInfo.removeAll()
            
            strongSelf.getSearchResults() { _ in
                self?.imagesTableView.cr.endHeaderRefresh()
            }
        }
        
        self.imagesTableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            guard let strongSelf = self else {return}
            let currentPage = strongSelf.metadata.currentPage
            let totalPages = strongSelf.metadata.totalPages
            
            if currentPage < totalPages {
                strongSelf.metadata.currentPage = strongSelf.metadata.currentPage + 1
                
                strongSelf.getSearchResults() { success in
                    strongSelf.imagesTableView.cr.endLoadingMore()
                    
                    if success && strongSelf.metadata.currentPage == strongSelf.metadata.totalPages {
                        strongSelf.imagesTableView.cr.noticeNoMoreData()
                    }
                }
            } else {
                /// Reset no more data
                strongSelf.imagesTableView.cr.noticeNoMoreData()
            }
        }
    }
}

