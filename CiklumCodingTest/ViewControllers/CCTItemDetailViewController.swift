//
//  CCTItemDetailViewController.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import UIKit

class CCTItemDetailViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var largeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var likeImageview: UIImageView!
    
    @IBOutlet weak var favoriteCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    @IBOutlet weak var tagListLabel: UILabel!
    
    // MARK: - Public properties
    var itemDetail: Hits!
    
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        updateViewWithDetail()
    }
    
    deinit {
        print("\(String(describing: CCTItemDetailViewController.self)) de-allocated")
    }
    
    // MARK: - Helper methods
    func updateViewWithDetail() {
        largeImageView.contentMode = .scaleAspectFit
        largeImageView.cct_setImage(withURL: itemDetail.largeImageURL, placeholderImage: UIImage(named: "placeholder-image") ) { [weak self] in
            self?.largeImageView.contentMode = .scaleAspectFill
        }
        
        favoriteImageView.image = UIImage(named: "favorite")?.withRenderingMode(.alwaysTemplate)
        commentImageView.image = UIImage(named: "comment")?.withRenderingMode(.alwaysTemplate)
        likeImageview.image = UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
        
        // Set color
        likeImageview.tintColor = UIColor.lightBlueColor
        commentImageView.tintColor = UIColor.lightBlueColor
        favoriteImageView.tintColor = UIColor.lightBlueColor
        
        title = itemDetail.user ?? ""
        favoriteCountLabel.text = "\(itemDetail.favorites ?? 0)"
        commentCountLabel.text = "\(itemDetail.comments ?? 0)"
        likeCountLabel.text = "\(itemDetail.likes ?? 0)"
        
        tagListLabel.text = itemDetail.formattedTags
    }
    
}
