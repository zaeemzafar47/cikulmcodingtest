//
//  Constants.swift
//  CiklumCodingTest
//
//  Created by Zaeem on 12/10/2018.
//  Copyright © 2018 ZZ All rights reserved.
//

import Foundation
import UIKit

typealias SimpleCompletionBlock = () -> ()
typealias CompletionBlockWithBool = (Bool) -> ()


struct CCTConstants {
    struct Network {
        static let baseURL = "https://pixabay.com"
        static let serverGenericError = "An error has occured"
        static let internetConnectionError = "Internet connection error, please check your network connection settings"
    }
    
    struct PublishableKeys {
        static let pixabayAPIKey = "10367762-6b32b54145ae9ad6bd5d3cdc4"
    }
    
    struct Storyboards {
        static let Main = UIStoryboard(name: "Main", bundle: nil)
    }
}
