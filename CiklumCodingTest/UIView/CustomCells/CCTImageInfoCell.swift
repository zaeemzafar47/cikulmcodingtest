//
//  CCTImageInfoCell.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import UIKit

class CCTImageInfoCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    // MARK: - Configure
    func configureCell(data: Hits) {
        nameLabel.text = data.user ?? ""
        
//        tagListView.removeAllTags()
//        tagListView.addTags(data.tagsStringArray)
        
        tagsLabel.text = data.formattedTags
        
        thumbnailImageView.cct_setImage(withURL: data.thumbnailURL, placeholderImage: UIImage(named: "placeholder-image"))
    }
    
}
