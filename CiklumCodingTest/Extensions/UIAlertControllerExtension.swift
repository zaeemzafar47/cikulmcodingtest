//
//  UIAlertControllerExtension.swift
//  CiklumCodingTest
//
//  Created by Zaeem on 12/10/2018.
//  Copyright © 2018 ZZ All rights reserved.
//

import UIKit

extension UIAlertController {
    
    class func showOkayAlert(inViewController: UIViewController, message: String, title: String, completion: (() -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK",
                                     style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                                        
                                        if completion != nil {
                                            completion!()
                                        }
        }
        
        alertController.addAction(okAction)
        inViewController.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlert(inViewController vc:UIViewController, title: String, message: String, okButtonTitle: String, cancelButtonTitle: String, isActionSheet: Bool = false, isButtonTypeDestructive: Bool = false, isSingleButtonAlert: Bool = false, completion:@escaping ((Bool) -> ())) {
        
        let alertController = UIAlertController (title: title, message: message, preferredStyle: isActionSheet ? .actionSheet : .alert)

        let settingsAction = UIAlertAction(title: okButtonTitle, style: isButtonTypeDestructive ? .destructive : .default) { (_) -> Void in
            completion(true)
        }
        
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (action) in
            completion(false)
        }
        
        if isSingleButtonAlert == false {
           alertController.addAction(settingsAction)
        }
        alertController.addAction(cancelAction)

        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func showSingleButtonAlert(inViewController vc:UIViewController, title: String, message: String, okButtonTitle: String, cancelButtonTitle: String, isActionSheet: Bool = false, completion:@escaping ((Bool) -> ())) {
        
        let alertController = UIAlertController()
        
        let settingsAction = UIAlertAction(title: okButtonTitle, style: .destructive) { (_) -> Void in
            completion(true)
        }
        
        
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (action) in
            completion(false)
        }
        
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertWithSettingsPrompt(title: String, message: String, fromViewController: UIViewController, completion: CompletionBlockWithBool? = nil) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            completion?(true)
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        let cancelAction = UIAlertAction(title: "Not now", style: .cancel, handler: { (_) -> Void in
            completion?(false)
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)

        fromViewController.present(alertController, animated: true, completion: nil)
    }
}
