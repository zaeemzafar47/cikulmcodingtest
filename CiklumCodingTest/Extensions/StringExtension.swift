//
//  StringExtension.swift
//  CiklumCodingTest
//
//  Created by ZaeemZafar on 12/10/2018.
//  Copyright © 2018 ZZ All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func equalIgnoreCase(_ compare:String) -> Bool {
        return self.uppercased() == compare.uppercased()
    }
    
    var removeDigits: String {
        return (self.components(separatedBy: .decimalDigits).joined(separator: ""))
    }
    
    var clear: String {
        return ""
    }
    
    
    func leftPadding(toLength: Int, withPad: String = " ") -> String {
        
        guard toLength > self.count else { return self }
        
        let padding = String(repeating: withPad, count: toLength - self.count)
        return padding + self
    }
    
    func validURLString(urlString: String?) -> Bool {
        guard let urlString = urlString,
            let url = URL(string: urlString) else {
                return false
        }
        
        return UIApplication.shared.canOpenURL(url)
    }
    
    func trimWhiteSpaces() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
