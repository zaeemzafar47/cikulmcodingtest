//
//  AlamofireImageExtension.swift
//  CiklumCodingTest
//
//  Created by ZaeemZafar on 12/10/2018.
//  Copyright © 2018 Zaeem. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

extension UIImageView {
    func cct_setImage(withURL: URL?, placeholderImage: UIImage?, showIndicator: Bool = true, completion: SimpleCompletionBlock? = nil) {
        if let imageURL = withURL {
            // Create an activity indicator from UIImageView frame - padding
            // Add as subview
            // Remove on completion
            
            let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicator.frame = bounds
            activityIndicator.tintColor = .red
            activityIndicator.hidesWhenStopped = true
            addSubview(activityIndicator)
            
            if showIndicator {
                activityIndicator.startAnimating()
            }
            
            af_setImage(withURL: imageURL, placeholderImage: placeholderImage) { (response) in
                activityIndicator.stopAnimating()
                completion?()
            }
            
        } else {
            self.image = placeholderImage
        }
    }
}
