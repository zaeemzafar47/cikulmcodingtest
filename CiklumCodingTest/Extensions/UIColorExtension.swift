//
//  ColorExtension.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import UIKit

extension UIColor {
    class var lightBlueColor: UIColor {
        return UIColor(red: 0, green: 128.0/255.0, blue: 1.0, alpha: 1.0)
    }
}

