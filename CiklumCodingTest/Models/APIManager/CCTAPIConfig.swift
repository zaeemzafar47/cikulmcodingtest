//
//  CCTAPIConfig.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import Foundation
import UIKit
import Alamofire


protocol CCTAPIConfig {
    var method: HTTPMethod { get }
    var path: String { get }
    var headers: [String: String]? { get }
    var parameters: Parameters? { get }
    var setAuth: Bool { get }
}
