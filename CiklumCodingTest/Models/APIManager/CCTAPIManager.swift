//
//  CCTAPIManager.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//
import Foundation

class CCTAPIManager: NSObject {
    
    // MARK: -
    
    static let shared: CCTAPIManager = {
        let instance = CCTAPIManager()
        
        return instance
    }()
    
    func sendRequest<T>(apiConfig: CCTAPIConfig, completion:@escaping (_ apiResponse: CCTAPIResponse<T>)->()) {
        
        CCTServerManager.shared.sendRequest(apiConfig: apiConfig) { (response) in
            var hasError = true
            var responseMessageString = CCTConstants.Network.serverGenericError
            let apiResponse = CCTAPIResponse<T>(success_status: true, messageString: "", dataModel: nil)
            if response.status {
                responseMessageString = response.message
                
                if response.statusCode() == CCTServerResponseCode.validResponse.rawValue {
                    hasError = false
                    
                } else if response.statusCode() == CCTServerResponseCode.failed.rawValue {
                    
                } else if response.statusCode() == CCTServerResponseCode.notFound.rawValue {
                    apiResponse.serverStatusCode = .notFound
                }
                
            } else {
                responseMessageString = response.customErrorMessage()
            }
            
            apiResponse.success = !hasError
            apiResponse.message = responseMessageString
            
            apiResponse.hasInternetAccess = CCTServerManager.isConnectedToInternet
            
            let jsonDecoder = JSONDecoder()
            
            do {
                if let responseData = response.response?.data {
                    apiResponse.data = try jsonDecoder.decode(T.self, from: responseData)
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
            completion(apiResponse)
        }
    }
}
