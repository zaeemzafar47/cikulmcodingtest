//
//  CCTEndPoints.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import Foundation
import UIKit
import Alamofire

enum CCTUserEndpoint: CCTAPIConfig {
    // API endpoints
    case searchImage(searchText: String, pageNumber: Int)
    
    
    // Protocol properties
    var method: HTTPMethod {
        switch self {
        case .searchImage:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .searchImage:
            // Search images don't have any path component except for query parameters
            return "/api/"
        }
    }
    
    var headers: [String : String]? {
        // Developer's Note: Here we can pass custom OR security information in header e.g. Authentication Token
        return nil
    }
    
    var parameters: Parameters? {
        
        switch self {
        case .searchImage(searchText: let searchText, let pageNumber):
            var params = [String: Any]()
            
            params["key"] = CCTConstants.PublishableKeys.pixabayAPIKey
            params["q"] = searchText
            params["page"] = pageNumber
            
            return params
        }
        
    }
    
    var setAuth: Bool {
        return false
    }
    
}
