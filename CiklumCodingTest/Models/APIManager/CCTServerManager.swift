//
//  CCTServerManager.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

// MARK: -

enum CCTServerRequestMethod: Int {
    case get = 0
    case post
    case put
    case delete
    case patch
    
    func toHTTPMethod() -> HTTPMethod {
        
        switch self {
        case .get:
            return .get
            
        case .post:
            return .post
            
        case .put:
            return .put
            
        case .delete:
            return .delete
            
        case .patch:
            return .patch
        }
    }
}

// MARK: -

struct CCTServerSettings {
    static let ExpiryTime: TimeInterval = 30 // in second
    static let ErrorCode = 1234567
    static let ErrorMessage = NSLocalizedString("An error occured. Please Try again.", comment: "")
    static let APIDomain = "com.iOSDeveloper.CiklumCodingTest.app.servermanager"
}

// MARK: -

typealias CCTServerManagerCompletionBlock = (_ response: CCTServerResponse) -> Void

// MARK: -

class CCTServerManager: SessionManager {
    
    // MARK: -
    
    fileprivate var baseURL: URL?
    
    // MARK: -
    
    static let shared: CCTServerManager = {
        let baseURL = URL(string: CCTConstants.Network.baseURL)
        let instance = CCTServerManager(baseURL: baseURL)
        
        return instance
    }()
    
    init(baseURL: URL?) {
        super.init(configuration: CCTServerManager.createSessionConfiguration(), delegate: SessionDelegate(), serverTrustPolicyManager: nil)
        
        var url = baseURL
        
        // Ensure terminal slash for baseURL path, so that NSURL relativeToURL works as expected
        if (((url?.path.count)! > 0) && !((url?.absoluteString.hasSuffix("/"))!)) {
            url = baseURL?.appendingPathComponent("")
        }
        
        self.baseURL = url
    }
    
    // MARK: -
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    @discardableResult
    func sendRequest(apiConfig: CCTAPIConfig,
                     completion: @escaping CCTServerManagerCompletionBlock) -> DataRequest {
        let fullURL = URL(string: apiConfig.path, relativeTo: baseURL)
        
        var requestHeaders = [String: String]()
        
        
        if apiConfig.headers?.isEmpty == false {
            
            for key in (apiConfig.headers?.keys)! {
                requestHeaders[key] = apiConfig.headers![key]
            }
        }
        
        let request = self.request(fullURL!,
                                   method: apiConfig.method,
                                   parameters: apiConfig.parameters,
                                   encoding: (apiConfig.method == .get) ? URLEncoding.default : JSONEncoding.default,
                                   headers: requestHeaders).responseJSON { (response) in
                                    
                                    
                                    print("Calling Endpoint: \(response.request?.url?.absoluteString ?? "EmptyURL")")
                                    print("Passed Parameters: \(String(describing: apiConfig.parameters))")
                                    print("raw response: \(response)")
                                    
                                    var serverResponse: CCTServerResponse!
                                    
                                    switch response.result {
                                        
                                    case .success(let value):
                                        var hasError = true
                                        var status = false
                                        var message = ""
                                        var resultError: Error? = nil
                                        var data: Any? = nil
                                        var serverStatusCode = 0
                                        var metaData = [String: Any]()
                                        
                                        let serverDataJson = JSON(value)
                                        
                                        if serverDataJson != JSON.null {
                                            status = true
                                            hasError = false
                                            
                                            if let serverMessage = serverDataJson["message"].string {
                                                message = serverMessage
                                            }
                                            
                                            data = serverDataJson["data"].dictionaryObject
                                            
                                            if let meta = serverDataJson["metadata"].dictionaryObject {
                                                metaData = meta
                                            }
                                            
                                            if data is NSNull {
                                                data = nil
                                            }
                                            
                                            if let statusCode = serverDataJson["statusCode"].int {
                                                serverStatusCode = statusCode
                                            }
                                        }
                                        
                                        if hasError == true {
                                            var userInfo = [String: Any]()
                                            userInfo[NSLocalizedDescriptionKey] = CCTServerSettings.ErrorMessage
                                            
                                            let anError = NSError.init(domain: CCTServerSettings.APIDomain, code: CCTServerSettings.ErrorCode, userInfo: userInfo)
                                            
                                            resultError = anError as Error
                                        }
                                        
                                        serverResponse = CCTServerResponse(status: status, serverStatusCode: serverStatusCode, response: response, metaData: metaData, result: data, error: resultError, message: message)
                                        
                                    case .failure(let error):
                                        serverResponse = CCTServerResponse(status: false, serverStatusCode: 0, response: response, metaData: [String: Any](), result: nil, error: error, message: "")
                                    }
                                    
                                    completion(serverResponse)
        }
        
        return request
    }
    
    // MARK: -
    
    class func createSessionConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = CCTServerSettings.ExpiryTime
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return configuration
    }
}
