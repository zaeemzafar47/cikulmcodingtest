//
//  CCTAPIResponse.swift
//  CiklumCodingTest
//
//  Created by Zaeem Zafar  on 12/10/2018.
//  Copyright © 2018 Zaeem Zafar . All rights reserved.
//

import Foundation


// MARK: -
class CCTAPIResponse<T: Codable>: Codable {
    var success = false
    var message = CCTConstants.Network.serverGenericError
    var serverStatusCode = CCTServerResponseCode.validResponse
    var hasInternetAccess = false
    var data: T?
    
    init(success_status: Bool, messageString: String, dataModel: T?) {
        success = success_status
        message = messageString
        data = dataModel
    }
}
