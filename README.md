# CikulmCodingTest

BUILD REQUIREMENTS
------------------------------
Swift 4.2+
Xcode 10.0+
iOS 11.0 SDK or later - Set as per our standard development rule to support current OS & N-1 version 


THIRD PARTY LIBRARIES/ FRAMEWORKS
------------------------------
Cocoapods dependency manager used to integrate following third party libraries

For correct version see Podfile.lock.

Alamofire                - For networking tasks
AlamofireImage      - Image caching 
SwiftyJSON            - Easy JSON parsing
MBProgressHUD    - Loading indicator  
CRRefresh              - Table header/footer refresh easy handling


Main Feature
------------------------------
Pixabay API Handlind and response caching 
List view required UI elements (Thumnai image, user name & tag list separated with '#')
Handled pagination and pull-to-refresh
Smart search implement 

